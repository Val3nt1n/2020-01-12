--  Insert 5 new employees with filled all of the fields (first name, last name..)
insert into angajati
(nume,prenume,id_departament)
values
('John','Mac','3'),
('Trisha','Lee','1'),
('Andrew','Bruce','2'),
('Jackson','Lee','1'),
('Mac','Jon','3');

--  Insert 1 new employee with current date of birth. Use builtin methods.  
insert into persoane 
(nume,prenume,data_nasterii)
values
('David','Lee',date(now()));

-- Update 3 recently inserted employees to have the same last name. 
update persoane set prenume = 'Jackson' order by id desc limit 3; 

-- Delete all employees, whos first name starts with „M”.
delete from persoane where nume = 'David';

select * from persoane;

