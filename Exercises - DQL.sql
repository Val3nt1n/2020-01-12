select prenume,nume,data_nasterii 
from persoane 
order by data_nasterii desc;

-- afisare primele 3 linii
select * from persoane limit 3;

-- afisare ultima persoana dintr-o lista
select * from persoane order by id desc limit 1; 

-- All employees, where department id is higher than 2. 
select * from angajati where id_departament > 2;

-- All first and last names from persons and employees in one table. 
select 
a.nume as 'Nume Angajati',
a.prenume as 'Prenume Angajati',
p.nume as 'Nume persoana',
p.prenume as 'Prenume persoana' 
from angajati as a,persoane as p;

-- select longest first name
select max(length(a.prenume)),max(length(p.prenume)) from persoane as p,angajati as a ;

-- total count of people
select count(*) from persoane;
