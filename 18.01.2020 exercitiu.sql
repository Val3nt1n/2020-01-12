create database universitate;

use universitate;

create table profesor (
id INT NOT NULL auto_increment primary key,
nume varchar(100) not null,
adresa varchar(200) not null
 );
 
create table departament (
id INT NOT NULL auto_increment primary key, 
nume varchar(100) not null
);


create table dep_prof (
id_prof int ,
id_dep int, 
foreign key (id_prof) references profesor(id),
foreign key (id_dep) references departament(id)
);

insert into profesor 
(nume,adresa)
values
('Belea Radu','Facultatea de electronica'),
('Chifan Mitica','Facultatea de stiinte'),
('Ivas Sergiu','Facultatea de Inginerie Electrica'),
('Cocu Adina','Facultatea de calculatoare faciee');

select * from profesor;

-- afisare structura tabelului
describe profesor;

-- modificare data type din coloana
alter table profesor 
modify COLUMN  nume varchar(200) not null;

alter table departament
modify column id int not null auto_increment primary key;

select * from departament;


insert into departament
(nume)
values
('Informatica'),
('Matematica'),
('Fizica'),
('Chimie'),
('Romana'),
('Electronica');

select * from de_prof;

insert into dep_prof
values
(1,3),
(1,5),
(2,2),
(2,1),
(3,5),
(3,6),
(3,3),
(4,1);

create table cursuri (
id int primary key auto_increment not null,
nume varchar(30),
credite int unsigned
);

insert into cursuri
(nume,credite)
values
('Java de la 0',6),
('Mysql',7),
('Rezistenta materialelor',3),
('Electronica digitala',5),
('Mecanica fluidelor',3),
('Mecanica digitala',5),
('Instrumentatie',1),
('Literatura universala',2),
('Tabelul lui Mendeleev',5),
('Algerbra descriptiva',6),
('Geometrie in spatiu',2);

select * from cursuri;
select * from departament;


alter table cursuri
add column id_dep int;

describe cursuri;

alter table cursuri
add constraint dep_fk foreign key (id_dep) references departament(id);

update cursuri set id_dep = 1 where id = 1;
update cursuri set id_dep = 1 where id = 2;
update cursuri set id_dep = 3 where id = 3;
update cursuri set id_dep = 1 where id = 4;
update cursuri set id_dep = 4 where id = 5;
update cursuri set id_dep = 6 where id = 6;
update cursuri set id_dep = 6 where id = 7;
update cursuri set id_dep = 5 where id = 8;
update cursuri set id_dep = 4 where id = 9;
update cursuri set id_dep = 2 where id = 10;
update cursuri set id_dep = 2 where id = 11;

create table student (
id int primary key auto_increment,
nume varchar(200),
id_prof int,
foreign key (id_prof) references profesor(id)
);

select * from profesor;

select * from student;

insert into student
(nume,id_prof)
values
('Abigale Bannister',1),
('Tomi Kemp',3),
('Emeli Mckee',2),
('Rishi Townsend',4),
('Anisha Hastings',4),
('Bertha Briggs',1),
('Ezra Lara',3);

create table cursuri_studenti (
id_stud int,
id_curs int,
foreign key (id_stud) references student(id),
foreign key (id_curs) references cursuri(id)
);

 select * from cursuri_studenti;
 select * from cursuri;
 
 insert into cursuri_studenti 
 values
 (1,2),
 (2,7),
 (3,1),
 (4,11),
 (4,5),
 (5,8),
 (5,6),
 (6,9),
 (6,4),
 (1,3),
 (7,10);
 
 select nume from departament;
 select * from profesor;
 select nume from profesor where nume like 'I%';
 select nume from profesor where nume like 'C%a' and nume like 'Cocu Adina';
 
alter table profesor
add column salariu int,
add column data_angajare date;

update profesor set salariu = 4000.00 , data_angajare = '1980-12-10' where id = 1 ;
update profesor set salariu = 3550.00 , data_angajare = '1995-3-9' where id = 2 ;
update profesor set salariu = 4200.00 , data_angajare = '1999-11-12' where id = 3 ;
update profesor set salariu = 5000.00 , data_angajare = '2002-8-5' where id = 4 ;
 
 select * from profesor order by id desc;
 
 select * from profesor where salariu < 3800.00;
 
 select * from profesor where salariu > 3500 and salariu < 4200;
 
 select salariu,nume from profesor where salariu between 3500 and 4900;
 
 
 select nume,data_angajare from profesor where month(data_angajare) = 8;
 
 select nume,data_angajare from profesor where day(data_angajare) = 9;
 
 select * from departament;
 select * from dep_prof;
 select * from profesor;
 
 select * from profesor 
 inner join dep_prof on profesor.id = id_prof 
 inner join departament on departament.id = departament.id
 where id_dep is not null;
 
 select p.nume as 'Profesor',d.nume as 'Departament' from profesor as p
 inner join dep_prof as dp on p.id = dp.id_prof
 inner join departament as d on d.id = dp.id_dep
 where d.nume like 'Info%';
 
  select p.nume as 'Profesor',d.nume as 'Departament' from profesor as p
 inner join dep_prof as dp on p.id = dp.id_prof
 inner join departament as d on d.id = dp.id_dep
 where d.nume like 'Mat%';
 
select count(p.nume) from profesor as p
inner join dep_prof as dp on p.id = dp.id_prof
inner join departament as d on d.id = dp.id_dep
where d.nume like 'Info%' ;


 