-- CONCAT(text1, text2, ...) - concatenates all arguments 
select concat(nume, ' - ', prenume) from persoane;

-- NOW() - returns current datetime 
select now();

--  IF(a, b, c) - returns b if a is true, and c if a is not true
select if(2>3,'true','false');

-- MOD(x, y) - returns x % y
select mod(2, 3);

--  YEAR(d), MONTH(d), DAY(d) - returns date part from a date/datetime passed as an argument 
select year(data_nasterii) from persoane;
